import React from 'react';

const PageNotFound = () => (
	<div>
		<h1 style={{
			textAlign: 'center',
			backgroundColor: 'white',
			margin: '20%',
			padding: '5%'
		}}>
			Not Found
		</h1>
	</div>
);

export default PageNotFound;
