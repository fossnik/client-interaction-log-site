import React, { Component } from 'react';
import _ from 'lodash';
import CreateInteraction from './interaction/create-interaction';
import InteractionList from './interaction/interaction-list';
import { Interaction } from '../models';
// import { DataStore, Predicates } from '@aws-amplify/datastore';

interface InteractionState {
	interactions: Interaction[];
}

const initialState: InteractionState = {
	interactions: new Array<Interaction>(),
};

export default class InteractionComponent extends Component<{}, InteractionState> {
	constructor(props: any) {
		super(props);
		this.state = initialState;
	}

	componentDidMount() {
		// this.loadTasks();
	}

	// async loadTasks() {
	//   let this = this;

	//   // Load interactions from DataStore
	//   let dataStoreItems = await DataStore.query(Interaction);
	//   let interactions = _.map(dataStoreItems, item => {
	//     return {
	//       id: item.id,
	//       task: item.task
	//     };
	//   });
	//   this.setState({ interactions });
	// }

	// subscribeForNewTasks() {
	//   DataStore.observe(Interaction).subscribe(task => {
	//     if (task.opType === 'INSERT') {
	//       this.state.interactions.unshift(task);
	//     }
	//   });
	// }

	render() {
		return (
			<div>
				<div className="row large-6 large-offset-5 medium-6 medium-offset-5 small-6 small-offset-5 columns">
					<h3>Interactions List</h3>
				</div>
				<CreateInteraction createInteraction={this.createInteraction.bind(this)}/>
				<InteractionList
					interactions={this.state.interactions}
					updateInteraction={this.updateInteraction.bind(this)}
					deleteInteraction={this.deleteInteraction.bind(this)}
				/>
			</div>
		);
	}

	async createInteraction(interaction: Interaction) {
		// Create interaction in DataStore
		// const newInteraction = await DataStore.save(
		//   new Interaction({
		//     task: task.task
		//   })
		// );
		// task.id = newInteraction[0].id;
		this.state.interactions.unshift(interaction);
		this.setState({ interactions: this.state.interactions });
	}

	async updateInteraction() {
		// Update DataStore
		// const original = await DataStore.query(Interaction, interactionId);
		// await DataStore.save(
		//   Interaction.copyOf(original, updated => {
		//     updated.task = interactionText;
		//   })
		// );
	}

	async deleteInteraction(deleteInteraction: Interaction) {
		_.remove(this.state.interactions, (i: Interaction) => i.id === deleteInteraction.id);
		this.setState({ interactions: this.state.interactions });

		// Remove from DataStore
		// const todelete = await DataStore.query(Interaction, deleteInteraction.id);
		// DataStore.delete(todelete);
	}
}
