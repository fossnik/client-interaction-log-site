import React, { useState } from 'react';
import { Interaction } from '../../models';

export default function InteractionListItem(
	props: {
		key: number;
		interaction: Interaction;
		deleteInteraction: (deleteInteraction: Interaction) => any;
		updateInteraction: (interactionId: any, interactionText: any) => any;
	}
) {
	const [isEditing, setIsEditing] = useState(false);
	const [statusText, setStatusText] = useState();

	function renderTaskSection() {
		const { interaction } = props;

		if (isEditing) {
			return (
				<td>
					<form onSubmit={onSaveClick}>
						<input type="text" defaultValue={interaction.status} onChange={e => setStatusText(e.target.value)}/>
					</form>
				</td>
			);
		}

		return <td style={{
			color: '#d35400',
			fontSize: '20px',
			cursor: 'pointer'
		}}>{interaction.status}</td>;
	}

	return (
		<tr>
			{renderTaskSection()}
			{renderActionSection()}
		</tr>
	);

	function renderActionSection() {
		const style = {
			marginRight: '5px',
			width: '75px'
		};

		if (isEditing) {
			return (
				<td>
					<button style={style} type="button" className="success button" onClick={onSaveClick}>
						Save
					</button>
					<button style={style} type="button" className="secondary button" onClick={onCancelClick}>
						Cancel
					</button>
				</td>
			);
		}

		return (
			<td>
				<button style={style} type="button" className="button" onClick={onEditClick}>
					{' '}
					Edit{' '}
				</button>
				<button style={style} type="button" className="alert button" onClick={onDeleteClick}>
					{' '}
					Delete{' '}
				</button>
			</td>
		);
	}

	function onEditClick() {
		setIsEditing(true);
	}

	function onCancelClick() {
		setIsEditing(false);
	}

	function onSaveClick(e: { preventDefault: () => void; }) {
		e.preventDefault();
		const interactionId = props.interaction.id;
		const interactionText = statusText;
		props.updateInteraction(interactionId, interactionText);
		setIsEditing(false);
	}

	function onDeleteClick(e: { preventDefault: () => void; }) {
		e.preventDefault();
		props.deleteInteraction(props.interaction);
	}
}
