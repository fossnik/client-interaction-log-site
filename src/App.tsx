import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import HomePage from './components/homePage';
import Interaction from './components/interaction';
import PageNotFound from './components/pageNotFound';

export default () => (
	<Router>
		<Switch>
			<Route exact path="/create_interaction" component={Interaction}/>
			<Route exact path="/" component={HomePage}/>
			<Route component={PageNotFound}/>
		</Switch>
	</Router>
);
