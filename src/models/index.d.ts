import { ModelInit, MutableModel, PersistentModelConstructor } from "@aws-amplify/datastore";



export declare class Interaction {
  readonly id: string;
  readonly status?: string;
  readonly description?: Note;
  readonly persons?: Person[];
  readonly corporations?: Corporation[];
  readonly personInteractionsId?: string;
  readonly corporationInteractionsId?: string;
  constructor(init: ModelInit<Interaction>);
  static copyOf(source: Interaction, mutator: (draft: MutableModel<Interaction>) => MutableModel<Interaction> | void): Interaction;
}

export declare class Note {
  readonly id: string;
  readonly content: string;
  readonly personNotesId?: string;
  readonly corporationNotesId?: string;
  constructor(init: ModelInit<Note>);
  static copyOf(source: Note, mutator: (draft: MutableModel<Note>) => MutableModel<Note> | void): Note;
}

export declare class Person {
  readonly id: string;
  readonly name: string;
  readonly employer?: string;
  readonly website?: string;
  readonly linkedin?: string;
  readonly notes?: Note[];
  readonly interactions?: Interaction[];
  readonly interactionPersonsId?: string;
  constructor(init: ModelInit<Person>);
  static copyOf(source: Person, mutator: (draft: MutableModel<Person>) => MutableModel<Person> | void): Person;
}

export declare class Corporation {
  readonly id: string;
  readonly name: string;
  readonly address?: string;
  readonly website?: string;
  readonly linkedin?: string;
  readonly notes?: Note[];
  readonly interactions?: Interaction[];
  readonly interactionCorporationsId?: string;
  constructor(init: ModelInit<Corporation>);
  static copyOf(source: Corporation, mutator: (draft: MutableModel<Corporation>) => MutableModel<Corporation> | void): Corporation;
}