// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';



const { Interaction, Note, Person, Corporation } = initSchema(schema);

export {
  Interaction,
  Note,
  Person,
  Corporation
};